# python_modwsgi

docker build --build-arg app_name=teste \
  --build-arg tier_name=teste \
  --build-arg node_name=teste \
  --build-arg controller_host=http://teste.com \
  --build-arg controller_port=80 \
  --build-arg ssl=false \
  --build-arg account_name=customer1 \
  --build-arg account_access_keys=43434876 \
  -f Dockerfile \
  -t caiquefq/python_modwsgi .


docker rm -f $(docker ps -a -q)
docker volume rm $(docker volume ls -q)

docker build -f Dockerfile -t caiquefq/python_modwsgi .
docker run -d -p 80:80 --name python_modwsgi caiquefq/python_modwsgi

a2enconf wsgi
a2ensite
systemctl restart apache2
pyagent proxy restart
/etc/apache2/sites-available
