# Set the base image
FROM ubuntu:latest

# File Author / Maintainer
LABEL maintainer="caique.queiroz@gmail.com"

ARG app_name
ARG tier_name
ARG node_name
ARG controller_host
ARG controller_port
ARG ssl
ARG account_name
ARG account_access_keys

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install tzdata -y
RUN apt-get install python3 python3-dev python3-pip python-setuptools -y
RUN apt-get install python3-venv -y
RUN apt-get install libexpat1 -y
RUN apt-get install apache2 -y
RUN apt-get install apache2-utils -y
RUN apt-get install ssl-cert -y
RUN apt-get install libapache2-mod-wsgi -y
RUN apt-get install apt-transport-https -y
RUN apt-get install ca-certificates -y
RUN apt-get install curl -y
RUN apt-get install gnupg -y
RUN apt-get install lsb-release -y
RUN apt-get install unzip -y
RUN apt-get install bash-completion -y
RUN apt-get install software-properties-common -y
RUN apt-get install net-tools -y
RUN apt-get install vim -y

COPY ./wsgi.py /var/www/html/
COPY ./wsgi.conf /etc/apache2/conf-available/
COPY ./000-default.conf /etc/apache2/sites-enabled/

RUN chown www-data:www-data /var/www/html/wsgi.py
RUN /usr/sbin/a2enconf wsgi
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN /etc/init.d/apache2 restart

RUN pip install -U appdynamics

RUN echo "[agent]\napp = ${app_name}\ntier = ${tier_name}\nnode = ${node_name}\n\n[controller]\nhost = ${controller_host}\nport = ${controller_port}\nssl = ${ssl}\naccount = ${account_name}\naccesskey = ${account_access_keys}\n\n[wsgi]\nscript = /var/www/html/wsgi.py\ncallable = wsgi" >> /etc/appdynamics.cfg
RUN pyagent proxy start

EXPOSE 80

WORKDIR /var/www/html

CMD /usr/sbin/apache2ctl -D FOREGROUND